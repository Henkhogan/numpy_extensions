import unittest
import numpy as np
from .arrays import simple_rec, simple_nd, a8_rec, a12_rec
from projects import numpy_extensions

ref_dtype =[('valueid', '<i8'), ('recordid', '<i8'), ('recordid__2', '<i8'), ('seriesid', '<i8'), ('seriesid__2', '<i8'), ('value', '<i8'), ('value__2', '<i8')]
ref_array = np.array([(1, 1, 2, 1, 2, 100, 200), (2, 3, 4, 1, 2, 150, 258)], dtype = ref_dtype)

class TestAll(unittest.TestCase):
	
	def simple(self):
		s = simple_rec.copy()
		numpy_extensions.add_custom_extensions(s)
		return s
		
	def unpivot(self):
		s = self.simple()
		u = s.custom_extensions.unpivot('valueid', 'seriesid')
		return u
		
	def test_init(self):
		numpy_extensions.CustomExtensions(self.simple())
		
	def test_extract_vectors_by_name(self):
		t = self.simple().custom_extensions.extract_vectors_by_name('recordid', 'seriesid', 'valueid', 'value')
		ref = [np.array([1, 2 ,3 ,4]), np.array([1, 2, 1, 2]), np.array([1, 1, 2, 2]), np.array([100, 200, 150, 258])]
		np.testing.assert_array_equal(t, ref)
		
	def test_recarray(self):
		t = numpy_extensions._recarray(simple_nd)
		s = self.simple()
		self.assertEqual(type(simple_nd),np.ndarray)
		self.assertEqual(type(t),np.core.records.recarray)
		self.assertEqual(type(s),np.core.records.recarray)
		np.testing.assert_array_equal(s,t)
	
	def test_unpivot(self):
		u = self.unpivot()
		np.testing.assert_array_equal(u,ref_array)
		
	def test_corrcoef(self):
		a1 = a8_rec.copy()
		numpy_extensions.add_custom_extensions(a1)
		u1 = a1.custom_extensions.unpivot('valueid', 'seriesid')
		c1 = u1.custom_extensions.correlations.corrcoef('value','value__2')
		print(c1)
		
		a2 = a12_rec.copy()
		numpy_extensions.add_custom_extensions(a2)
		u2 = a2.custom_extensions.unpivot('valueid', 'seriesid')
		c2 = u2.custom_extensions.correlations.corrcoef('value','value__2','value__3')
		print(c2)
