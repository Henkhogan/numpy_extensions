import numpy as np
			
def generate_list(a=4, b=100):
	record_list = []
	for i in range(0,a):
		for l in range(0,b):
			recordid = i*b+l
			seriesid = i
			valueid = l
			p1 = abs(i-a/2)
			p2 = ((-1)**l)
			p3 = ((-1)**i)
			p4 = l
			value = p1 * p2 * p3 * p4
			record_list.append(
				(recordid, seriesid, valueid, value)			
				)
	return record_list
		
def static_and_range(end= 100, factor=1 ,start=0, static=0):
	return np.core.records.fromrecords([(static,x*factor) for x in range(start, end)], names=('v0', 'v1'))


fields = ['recordid', 'seriesid', 'valueid', 'value']

_very_simple_records = [
	(1,1,1,100),
	(2,2,1,200)
	]
	
_simple_records = _very_simple_records + [
	(3,1,2,150),
	(4,2,2,258)
	]
	
_8 = _simple_records + [
	(5,1,3,550),
	(6,2,3,280),
	(7,1,4,750),
	(8,2,4,310)	
	]
	
_12 = _8 + [
	(9,3,1,80),
	(10,3,2,40),	
	(11,3,3,80),
	(12,3,4,40),	
	]

very_simple = np.core.records.fromrecords(_very_simple_records, names=fields)

simple_rec = np.core.records.fromrecords(_simple_records, names=fields)

simple_nd = np.array(_simple_records, dtype=simple_rec.dtype)

a8_rec = np.core.records.fromrecords(_8, names=fields)

a12_rec = np.core.records.fromrecords(_12, names=fields)

big = np.core.records.fromrecords(generate_list(), names=fields)

