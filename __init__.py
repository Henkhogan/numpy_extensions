import numpy as np
import numpy.lib.recfunctions as rfn

from cloud_libs.pythonista_helper.baseLogger import getLogger
logger = getLogger(__file__)

def _recarray(numpy_array):
	return np.core.records.fromrecords(numpy_array.tolist(), dtype=numpy_array.dtype)

def _unpivot(numpy_array, shared_field_name, value_field_name, value_field_postfix = 'value__', drop_extra_field:bool = False):
	
	value, value_pos = np.unique(numpy_array[value_field_name], return_inverse=True)
	shared, shared_pos = np.unique(numpy_array[shared_field_name], return_inverse=True)
	
	joined_array = None
	for v in value:
		filter_array = numpy_array[np.where(numpy_array[value_field_name] == v)]
		if joined_array == None:
			joined_array = filter_array
		else:
			joined_array = rfn.join_by(shared_field_name, joined_array, filter_array, jointype='inner', usemask=False, r1postfix='' ,r2postfix = '__'+str(v))
	if joined_array == None:
		raise ValueError('join failed')
	return _recarray(joined_array)
	

class CustomExtensionSubclass:
	def _set_array(array):
		raise Exception('read-only attribute array can not be set')
	
	def _get_array(self):
		return self.custom_extension.array
	
	array = property(_set_array,_get_array)
	
	def __init__(self, custom_extensions):
		self.custom_extensions = custom_extensions
		
class Correlations(CustomExtensionSubclass):		
		
	def corrcoef(self, *args,):
		x = self.custom_extensions.extract_vectors_by_name(*args)
		return np.corrcoef(x,rowvar=1)
		
class CustomExtensions:
	def __init__(self, array):
		self.array = array
		self.correlations = Correlations(self)
	
	def extract_vectors_by_name(self,*names,tolist=False):
		if tolist:
			return [self.array[x].tolist() for x in names]	
		return [self.array[x] for x in names]
	
	def recarray(self):
		return _recarray(self.array)
		
	def unpivot(self, shared_field, value_field):
		u = _unpivot(self.array, shared_field, value_field)
		add_custom_extensions(u)
		return u
		

def add_custom_extensions(numpy_array):
	if type(numpy_array)!=np.core.records.recarray:
			logger.debug('completed recarray conversion')
			numpy_array = _recarray(numpy_array)
	try:
		setattr(numpy_array, 'custom_extensions', CustomExtensions(numpy_array))
		logger.debug(f'added custom extension on type {type(numpy_array)}')
	except Exception:
		logger.exception(f'could not set attribute on type {type(numpy_array)}')
