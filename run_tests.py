import os  
import sys
import logging
import unittest
from importlib import reload

import testApp


logger = logging.getLogger('numpy_extensions')
level = logging.DEBUG
logger.setLevel(level)
logger.root.setLevel(level)

if __name__ == '__main__':
	this_dir = os.path.dirname(__file__)
	parent_dir = os.path.dirname(this_dir)
	sys.path[0] = parent_dir
	print(sys.path[0])
	unittest.main('testApp.tests')
