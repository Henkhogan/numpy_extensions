import os
#from distutils.core import setup
from setuptools import find_packages, setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup_kwargs = {
	'name' : "numpy_extensions",
	'version' : "0.0.1",
	'author' : "Henk Hogan",
	'author_email' : "henkhogan@gmail.com",
	'description' : ("The author is lazy"),
	'license' : "N/A",
	'keywords' : "N/A",
	'url' : "N/A",
	'packages' : find_packages(),
	'install_requires' : ['numpy'],
	'long_description' : read('README'),
	'classifiers' : [
		"Development Status :: 3 - Alpha",
		"Topic :: Utilities",
		"License :: OSI Approved :: BSD License",
		],
}

setup(**setup_kwargs)
